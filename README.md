# mobian-blog

```mermaid
graph LR
A(Generate post) --> B((Write article))
B --> C(Build pages)
```
## How ?

`git clone https://gitlab.com/mobian1/mobian-blog.git`

`cd mobian-blog/`

`hugo server`

## To add a post :

`hugo new posts/my-first-post.md`

Edit with your favorite editor and change the draft value to true when ok to publish.
You should also add the post tags manually.

For example :
>tags = ["mobian", "blog"]

## To build static pages :

`hugo -D` 

You'll find them in public folder (gitignored folder)

[More info](https://gohugo.io/getting-started/quick-start/)
