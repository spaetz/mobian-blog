+++
title = "Game Changers"
date = 2022-02-04T00:00:00+00:00
tags = ["pine64", "pinephone", "purism", "librem5", "floss"]
author = "a-wai"
+++

While mildly working on and playing with the
[PinePhone Pro]({{< ref "pinephone-pro.md" >}}), I recently got amazed at how
pleasant to use this device was, and how little work it took us to get to this
point!

<!--more-->

# Clever choices

Don't get me wrong, the fact that the
[PinePhone Pro](https://www.pine64.org/pinephonepro/) is already quite usable
(to the point where the bravest could probably daily-drive it -- I actually did
for a whole week!) is the result of countless hours of engineering, most of
those having happened before the Developer Edition even shipped, in good part
thanks to the combined efforts of [@megi](http://xnux.eu/) and
[@MartijnBraam](https://mastodon.online/web/@martijnbraam@fosstodon.org).

It's also the result of very clever choices made by
[Pine64](https://www.pine64.org):
- design the device around the RockChip RK3399, a very well-known and supported
  SoC in mainline Linux and u-boot
- use the modem we already know (and, at times, hate) from the
  [OG PinePhone](https://www.pine64.org/pinephone/)
- choose peripheral chips identical to those used on the OG PinePhone
  (flashlight, accelerometer...) or already well supported upstream (audio
  codec)

This definitely eased the initial bringup process and allowed us to enable
several useful features very quickly. However, the most amazing part of this
whole process is the maturity of the whole software ecosystem when compared
to what we experienced in the days of the BraveHeart Edition PinePhone!

# If you build it, they will come

Two years ago, when first starting working on the OG PinePhone, the available
software targeting mobile phones was a bit rough around the edges, to say the
least:
- [Phosh](https://puri.sm/posts/phosh-overview/) was barely past the
  proof-of-concept stage
- [Plasma Mobile](https://plasma-mobile.org/) wasn't very usable, although
  things improved quickly
- mobile-friendly apps were few, and often needed a number of downstream
  patches to be usable on phones

With the exception of [UBPorts](https://ubports.com/)/Lomiri, the FLOSS
ecosystem clearly wasn't ready for the PinePhone! However, give enough
developers an itch to collectively scratch, and they'll each do their part.

And that's *precisely* what made the PinePhone instrumental in improving the
mobile Linux ecosystem: by providing a low-cost (yet usable) device able to run
a "real" (as opposed to Android, for example) Linux system, it ended up in the
hands of many FLOSS developers, each with their own wishes and requirements.
This obviously resulted in more available brains to work on those matters, and
accelerated the development of software targeting mobile phones.

Not every PinePhone owner was a developer though, and non-dev users also did
help improving the ecosystem: first by bugging us enough so we would address
(and usually fix) their problems, but also by simply increasing the demand for
mobile-friendly and/or adaptive applications. This prompted more developers of
pre-existing apps to ensure their software would be usable on our tiny,
touchscreen-based devices.

# With great power, comes great software

While access to hardware and applications availability are both important for
Linux to successfully conquer mobile devices, building a full mobile
environment is a huge task, not easily achieved by relying only on volunteers'
efforts.

Enters [Purism](https://puri.sm): unlike Pine64, they actually have a number of
highly skilled software developers and designers on their payroll. That alone
made a huge difference for the whole ecosystem: instead of focusing solely on
their own [Librem 5](https://puri.sm/products/librem-5/), Purism employees did
(and continue to) spend their working hours developing Phosh and its siblings
in the open, giving mobile users a usable, fast-evolving, community-friendly
graphical environment they could use on any decent smartphone running mainline
Linux.

Likewise, mobile devices need applications, and Purism led the way on that front
too: by working on [libhandy](https://gitlab.gnome.org/GNOME/libhandy), then
[libadwaita](https://gitlab.gnome.org/GNOME/libadwaita) and pushing for their
adoption by the wider GNOME community, they paved the way for a brighter future
for both mobile and desktop Linux users.

In the end, the software ecosystem initially developed for the Librem 5 has been
widely adopted by the mobile Linux community and made its way into most of the
distributions targeting such devices. The way all those building blocks evolved
over the past two years, thanks to the combined efforts of volunteers and paid
developers, now allows us to provide an enjoyable user experience with minimal
efforts, even on a new device such as the PinePhone Pro!

# Complement, not compete

As the Librem 5 and PinePhone (including the Pro version) are the only
Linux-first smartphones currently available for new purchases, people tend to
compare those directly, more often than not criticizing Purism for the
(admittedly high) price of their smartphone. Those comparisons generally miss
the point by ignoring the different cost structure for each device, and
specifically the cost of software engineering Purism is funding (trust me,
engineering time is not cheap).

But more importantly, those are not random smartphones, designed with planned
obsolescence in mind and eager to capture every single bit of data your digital
life can generate. Both the Librem 5 and the PinePhone are privacy-respecting,
empowering smartphones, and complement each other very well:
- PinePhone users provide a large enough user base so bugs can be tracked down
  and fixed over time
- PinePhone developers contribute fixes and features to the Phosh software
  stack
- Librem 5 users indirectly fund the software development effort
- Librem 5 developers provide the whole community with quality software and
  lead the GNOME transition to mobile-friendly applications

Whatever device you may choose and/or prefer, both should be acknowledged for
what they are: game changers which helped shape the mobile Linux community
each in their own way, and will likely keep doing so for many years to come.
