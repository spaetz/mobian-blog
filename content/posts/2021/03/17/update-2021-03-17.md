+++
title = "2021-03-17 Status update"
date = 2021-03-17T00:00:00+00:00
tags = ["Mobian", "update", "development"]
+++

In the past few months, a whole lot has happened for Mobian: we launched, in
close cooperation with [Pine64](https://www.pine64.org/), the [Mobian Community
Edition PinePhone]({{< ref "mobian-community-edition.md" >}}),
which by now should have reached the hands of almost all who ordered it. We went
through several iterations of our on-device installer and other strategic
packages, such as the kernel, and we've been busy improving both our packages
and infrastructure.

<!--more-->

## Mobian CE and related challenges

We're very grateful that Pine64 offered us the opportunity to put Mobian into
the hands of PinePhone owners as the factory default OS for the last batch of
"Community Edition" PinePhones. It drove us to work tirelessly in order to make
Mobian a great out-of-the-box experience, and prompted us to work hard on the
on-device installer and related improvements.

Due to production constraints, the Mobian CE PinePhones shipped with version
`alpha2` of the installer, which was built on December 2nd, 2020. These phones
started shipping more than 2 months later, and therefore couldn't benefit from
several improvements we made in-between.

While we did our best to make sure the initial upgrade went fine, we got bit by
an issue related to a `pam` upgrade in upstream Debian which [broke the screen
unlocking]({{< ref "pam_issue.md" >}}),
requiring special attention during the first system update.

We also uncovered an issue where the `/boot` partition would quickly be filled
due to a kernel version upgrade from 5.10 to 5.11 when full disk encryption was
enabled. This is due to the unusually large initramfs file size as it must carry
all software required for unlocking the device, including graphics drivers and
libraries. We mitigated this issue by forcing `zstd` compression of the
initramfs and improving the initramfs generation to only include required
graphics drivers. However, major upgrades still require special attention as the
`/boot` partition is still a bit small and could easily be filled.

We increased the size of this partition on all the recently generated images, so
if you want permanent peace of mind, we suggest you flash your PinePhone with
the latest reference image or installer.

## Modem and hardware support improvements

During these past few months, we greatly improved `eg25-manager`, including
valuable contributions from [postmarketOS](http://postmarketos.org/),
[UBports](https://www.ubports.com) and [Plasma
Mobile](https://www.plasma-mobile.org/) developers. It is now more stable and
can recover more easily from unusual situations. Another great improvement is
you can now use custom command sequences for modem configuration, suspend and
resume: all of those are now stored in configuration files users can edit and
override without having to recompile the whole software.

We also published a 5.11 kernel which isn't the default yet, but can be
installed with `apt install linux-image-5.11-sunxi64`. This kernel vastly
improves charging support using USB PD, and is confirmed to work just fine with
the [PinePower Desktop](https://www.pine64.org/pinepowerdesktop/)'s USB-C port.

## Debian bullseye freeze and Mobian

Debian bullseye went through several stages of its freeze process, starting
January 12th. This reduced the flow of updates a bit, and Mobian's base system
won't evolve much in the coming weeks. The current Mobian repository will keep
tracking bullseye, meaning our packages will only get selective updates from now
on, and require extra care to not break anything.

Once bullseye is released, the current Mobian version will then be considered
"stable". This won't mean all bugs have been fixed, but rather that updates will
likely never break your system.

This also means some packages won't be updated anymore when they start requiring
upgrades of important underlying libraries. One good example is phoc, which for
now builds against version 0.11 of `wlroots`, but will evolve in the future to
only support more recent versions of `wlroots`. When that happens, we won't be
able to upgrade phoc anymore in the current repository.

As Mobian is still heavily a work-in-progress, we don't want to stop the flow of
fixes and improvements though, which is why we created the
[staging]({{< ref "unstable-distro.md" >}})
repository. All package updates now go into `staging` first, and are manually
migrated to the bullseye archive after a few days of testing. This will
participate in bringing you more reliable system upgrades.

## Improved on-device installer and new images

The installer has also seen a lot of work, and now includes the ability to
flash the installer to an SD card, boot from the card, and install Mobian to the
internal eMMC. It also allows you to select the root partition filesystem, with
a choice of ext4 or F2FS (which has been, by the way, the default filesystem
for nightly images for the last month).

We are therefore releasing a
[new version](https://images.mobian.org/pinephone/installer/) of the
installer today, which includes all the latest improvements to both the
installer itself and the base image.

A new reference image for the PinePhone is also available
[for download](https://images.mobian.org/pinephone/).

And as usual, we don't forget the PineTab, which also has its own new
[installer](https://images.mobian.org/pinetab/installer/) and
[reference images](https://images.mobian.org/pinetab/).

Enjoy :)
